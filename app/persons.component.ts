import {Component} from 'angular2/core';
import {Person} from './person';

@Component({
    selector: 'personList',
    template: `<ul>
                   <li *ngFor="#person of persons">
                        {{person.id}} - {{person.name}}
                   </li>
               </ul>`    
})

export class PersonsComponent {
    persons = PERSONS;
}

var PERSONS: Person[] = [
        { "id": 11, "name": "Tomas Kloucek" },
        { "id": 12, "name": "Donald Trump" },
        { "id": 13, "name": "Barack Obama" },
];


