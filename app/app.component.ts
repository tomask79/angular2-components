import {Component} from 'angular2/core';
import {PersonsComponent} from './persons.component';

@Component({
    selector: 'my-app',
    template: `<h1>Angular2 component with child component!</h1>
                <personList></personList>
              `,
    directives: [PersonsComponent]
})
export class AppComponent { 

}