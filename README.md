# Writing applications in Angular 2 [part1] #

Okay, I finally found a time to go through the features of [Angular 2](https://angular.io/) framework and test some of them. 
And I have to say that I'm impressed! Because from the first tutorials and guides it's obvious that Angular team took the best from
[React JS](https://facebook.github.io/react/) like splitting the application into multiple components and [Reactive system](http://victorsavkin.com/post/110170125256/change-detection-in-angular-2) for checking the changes in DOM and they added to it the only good thing(IMHO) from Angular1 which is the [Form API](https://docs.angularjs.org/guide/forms). 

## Major changes in Angular 2 (from my point of view) ##

* **No more controllers**. I'm not missing them, we've got new incredibly easy to use system of components.
* **$scope is dead**. We don't need it, because properties are bound to components. Finally will be clear what's bound to what.
* [Typescript](https://en.wikipedia.org/wiki/TypeScript) is highly welcomed. Bu you don't have to use it. You can stay with ES5, ES6 or Dart.

## Creating of Angular 2 component ##

I already prepared for you configured Angular 2 project with configured **packages.json** and **typescript** definition file.
"Npm start" will also launch lite server, so you can test your code immediatelly. So let's focus strictly on Angular 2.

### Index.html and Angular2 bootstraping ###

Let's go through the only HTML we will have:

```
<html>

  <head>
    <title>Angular 2 QuickStart</title>

    <!-- 1. Load libraries -->
    <!-- IE required polyfills, in this exact order -->
    <script src="node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="node_modules/systemjs/dist/system-polyfills.js"></script>

    <script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script>
    <script src="node_modules/rxjs/bundles/Rx.js"></script>
    <script src="node_modules/angular2/bundles/angular2.dev.js"></script>

    <!-- 2. Configure SystemJS -->
    <script>
      System.config({
        packages: {        
          app: {
            format: 'register',
            defaultExtension: 'js'
          }
        }
      });
      System.import('app/boot')
            .then(null, console.error.bind(console));
    </script>

  </head>

  <!-- 3. Display the application -->
  <body>
    <my-app>Loading...</my-app>
  </body>

</html>
```
* We imported **SystemJS** for JS files loading and importing.
* We imported **RxJS** for working with observables.
* And of course own **angular2.dev.js**.

Most important is the following line:

```
System.import('app/boot')
            .then(null, console.error.bind(console));
```

By this line you're telling Angular2 what is your main bootstrap file. Bootstrap file is the place where you insert your main root component. 
**System.import('app/boot') means that in the app folder we've got **boot.ts** file with root component placement.** Let's take a look on that file:

```
import {bootstrap}    from 'angular2/platform/browser'
import {AppComponent} from './app.component'

bootstrap(AppComponent);
```
Focus on the line: **import {AppComponent} from './app.component'** . By this line you're telling Angular 2 that you've got **app.component.ts** 
file in the same folder. By calling bootstrap(AppComponent) you're bootstraping your root component. And where exactly is going to be placed 
our root component in HTML? Every Angular2 component defines thing called **selector** which tells Angular2 name of the tag for that component.
Our root component has selector named "my-app", as you'll see later. In our index.html we placed it in here:

```
.
.
  <!-- 3. Display the application -->
  <body>
    <my-app>Loading...</my-app>
  </body>

```

### Angular2 root component ###

Angular2 is based on modularity. Strictly said every component is a class module with metadata. 
Okay, let's define our root component:

```
import {Component} from 'angular2/core';
import {PersonsComponent} from './persons.component';

@Component({
    selector: 'my-app',
    template: `<h1>Angular2 component with child component!</h1>
                <personList></personList>
              `,
    directives: [PersonsComponent]
})
export class AppComponent { 

}
```
* Code **(import {PersonsComponent} from './persons.component')** says: "Hey angular2, I've got persons.component.ts file (**do not put 'ts' into import!**) 
file with another component and I want to reference it in this component by name PersonsComponent.

* We also defined **Component decorator** where we said that our component will be placed under **my-app tag** (**selector**) and it will spit out template defined under
template field. By personList tag we're inserting another child component into our main component. To actually tell Angular2 to use it, we have to reference it via
**directives** field. 

### Child component ###

Let's define mentioned PersonsComponent

```
import {Component} from 'angular2/core';
import {Person} from './person';

@Component({
    selector: 'personList',
    template: `<ul>
                   <li *ngFor="#person of persons">
                        {{person.id}} - {{person.name}}
                   </li>
               </ul>`    
})

export class PersonsComponent {
    persons = PERSONS;
}

var PERSONS: Person[] = [
        { "id": 11, "name": "Tomas Kloucek" },
        { "id": 12, "name": "Donald Trump" },
        { "id": 13, "name": "Barack Obama" },
];
```
What this does is pretty much clear. It prints out list of people saved in persons attribute of PersonsComponent.
Pretty much easy, anyway I just wanted to show you how $scope is not needed anymore, because everything is binded to our class module.

### Test and launch the demo ###

* git clone <this repo>
* npm install (in the angular2-components directory)
* npm start
* Visit http://localhost:3000

Hope you found this usefull. Angular2 looks really promising from my point of view.
Especially in combination with TypeScript developers can be really productive with Angular2.

see you

Tomas